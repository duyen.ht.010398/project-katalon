<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>login by EXCEL</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>d6cb3fff-0769-4103-9e98-b9785046156b</testSuiteGuid>
   <testCaseLink>
      <guid>170d2f8b-d8b2-43a6-a8b5-3a3668215ad8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC Login by many account</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>8fadbae8-aeca-4473-8ec5-7eb459a64959</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/data excel</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>8fadbae8-aeca-4473-8ec5-7eb459a64959</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>1d22732e-e336-41ea-8b68-6dcf41e2eaf8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>8fadbae8-aeca-4473-8ec5-7eb459a64959</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>baa8ec11-164b-4643-be2f-7c8c9c2cb2c9</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
