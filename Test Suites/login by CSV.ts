<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>login by CSV</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>f95dd347-5722-4ebb-a0b1-5115fe7ed04c</testSuiteGuid>
   <testCaseLink>
      <guid>170d2f8b-d8b2-43a6-a8b5-3a3668215ad8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC Login by many account</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>9d0ea257-701a-4fc3-b3e8-b63c4f311129</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/data csv</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>9d0ea257-701a-4fc3-b3e8-b63c4f311129</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>﻿username</value>
         <variableId>1d22732e-e336-41ea-8b68-6dcf41e2eaf8</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>9d0ea257-701a-4fc3-b3e8-b63c4f311129</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>baa8ec11-164b-4643-be2f-7c8c9c2cb2c9</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
